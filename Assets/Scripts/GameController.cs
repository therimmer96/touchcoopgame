﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;




public class GameController : MonoBehaviour
{
	[SerializeField]
	List<Transform> draggableObjects;
	List<int> dragIndex;
	List<Vector3> dragOffset;

	[SerializeField]
	List<Transform> tappableObjects;


	[Header("Prefabs")]
	[SerializeField]
	GameObject longHorizontal;
	[SerializeField]
	GameObject longVertical;
	[SerializeField]
	GameObject weight;
	[SerializeField]
	GameObject goalCube;

	void OnEnable()
	{
		IT_Gesture.onDraggingStartE += OnDraggingStart;
		IT_Gesture.onDraggingE += OnDragging;
		IT_Gesture.onDraggingEndE += OnDraggingEnd;

		IT_Gesture.onMultiTapE += OnMultiTap;

	}

	void OnDisable()
	{
		IT_Gesture.onDraggingStartE -= OnDraggingStart;
		IT_Gesture.onDraggingE -= OnDragging;
		IT_Gesture.onDraggingEndE -= OnDraggingEnd;

		IT_Gesture.onMultiTapE -= OnMultiTap;
	}

	// Use this for initialization
	void Start()
	{
		if (draggableObjects == null)
			draggableObjects = new List<Transform>();

		dragIndex = new List<int>();
		dragOffset = new List<Vector3>();


		for (int i = 0; i < draggableObjects.Count; i++)
		{
			dragIndex.Add(-1);
			dragOffset.Add(Vector3.zero);
		}

		if (dragIndex.Count != draggableObjects.Count)
		{
			Log.AddError("dragIndex.Count = " + dragIndex.Count + " | draggableObjects.Count = " + draggableObjects.Count);
		}
	}

	#region Touch Stuff

	private void OnMultiTap(Tap tap)
	{
		//do a raycast base on the position of the tap
		Ray ray = Camera.main.ScreenPointToRay(tap.pos);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		{
			foreach (var item in tappableObjects)
			{
				//if the tap lands on the shortTapObj, then shows the effect.
				if (hit.collider.transform == item)
				{
					if (item.gameObject.name == "BTN_Refresh")
					{

						Refresh();
					}
					else if (item.gameObject.name == "LongHorizontalSel")
					{
						NewLongHorizontal();
					}
					else if (item.gameObject.name == "LongVerticalSel")
					{
						NewLongVertical();
					}
					else if (item.gameObject.name == "WeightSel")
					{
						NewWeight();
					}
					else if (item.gameObject.name == "GoalCubeSel")
					{
						NewGoalCube();
					}
				}
			}
		}
	}

	

	void OnDraggingStart(DragInfo dragInfo)
	{

		//currentDragIndex=dragInfo.index;

		//if(currentDragIndex==-1){
		Ray ray = Camera.main.ScreenPointToRay(dragInfo.pos);
		RaycastHit hit;
		//use raycast at the cursor position to detect the object
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		{
			Log.Add(draggableObjects.Count.ToString());

			for (int i = 0; i < draggableObjects.Count; i++)
			{

				if (hit.collider.transform == draggableObjects[i])
				{

					Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(dragInfo.pos.x, dragInfo.pos.y, 30));
					dragOffset[i] = draggableObjects[i].position - p;

					//latch dragObj1 to the cursor, based on the index
					ObjectToCursor(dragInfo, draggableObjects[i], i);
					dragIndex[i] = dragInfo.index;
				}
			}
		}
		//}
	}

	//triggered on a single-finger/mouse dragging event is on-going
	void OnDragging(DragInfo dragInfo)
	{
		for (int i = 0; i < dragIndex.Count; i++)
		{
			//if the dragInfo index matches dragIndex[i], call function to position object accordingly
			if (dragInfo.index == dragIndex[i])
			{
				ObjectToCursor(dragInfo, draggableObjects[i], i);
			}
		}
	}

	void ObjectToCursor(DragInfo dragInfo, Transform objectToLatch, int i)
	{
		Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(dragInfo.pos.x, dragInfo.pos.y, 30));
		objectToLatch.position = p + dragOffset[i];
	}

	void OnDraggingEnd(DragInfo dragInfo)
	{
		for (int i = 0; i < dragIndex.Count; i++)
		{
			if (dragInfo.index == dragIndex[i])
			{
				dragIndex[i] = -1;

				Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(dragInfo.pos.x, dragInfo.pos.y, 30));
				draggableObjects[i].position = p + dragOffset[i];

				draggableObjects[i].GetComponent<Rigidbody>().isKinematic = false;

				draggableObjects[i].GetComponent<Rigidbody>().useGravity = true;

				draggableObjects[i].GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
				draggableObjects[i].GetComponent<MeshRenderer>().receiveShadows = true;


				draggableObjects[i].transform.position = new Vector3(draggableObjects[i].transform.position.x, draggableObjects[i].transform.position.y, 0);

				draggableObjects.RemoveAt(i);
				dragIndex.RemoveAt(i);
				dragOffset.RemoveAt(i);

			}
		}
	}
	#endregion


	public void Refresh()
	{
		SceneManager.LoadScene("_MainScene", LoadSceneMode.Single);
	}
	private void NewLongHorizontal()
	{
		AddDraggable(Instantiate(longHorizontal));
	}

	private void NewLongVertical()
	{
		AddDraggable(Instantiate(longVertical));
	}

	private void NewWeight()
	{
		AddDraggable(Instantiate(weight));
	}

	private void NewGoalCube()
	{
		AddDraggable(Instantiate(goalCube));
	}

	private void AddDraggable(GameObject item)
	{
		draggableObjects.Add(item.transform);

		dragIndex.Add(-1);
		dragOffset.Add(Vector3.zero);
	}
}
