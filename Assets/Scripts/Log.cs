﻿using UnityEngine;
using System;

public class Log : MonoBehaviour
{
	public static void Add(string logLine)
	{
		Debug.Log("[" + DateTime.Now.ToString() + "] " + logLine);
	}

	public static void AddError(string logLine)
	{
		Debug.LogError("[" + DateTime.Now.ToString() + "] " + logLine);
	}
}
